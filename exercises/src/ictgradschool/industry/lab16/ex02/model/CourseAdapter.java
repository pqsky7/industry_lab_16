package ictgradschool.industry.lab16.ex02.model;


import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class CourseAdapter extends AbstractTableModel implements CourseListener {

    /**********************************************************************
     * YOUR CODE HERE
     */

    private Course model;
    private String[] title = {"Student ID","Surname","Forename","Exam","Test","Assignment","Overall"};

    public CourseAdapter(Course model) {
        this.model = model;
        model.addCourseListener(this);
    }

    @Override
    public String getColumnName(int column) {
        return title[column];
    }

    @Override
    public int getRowCount() {
        return model.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        StudentResult studentResult = model.getResultAt(rowIndex);

        switch (columnIndex) {
            case 0:
                return studentResult._studentID;
            case 1:
                return studentResult._studentSurname;
            case 2:
                return studentResult._studentForename;
            case 3:
                return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Exam);
            case 4:
                return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Test);
            case 5:
                return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Assignment);
            case 6:
                return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Overall);
        }
        return null;
    }

    @Override
    public void courseHasChanged(Course course) {
        fireTableDataChanged();
    }
}